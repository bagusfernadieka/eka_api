<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        $currentDateTime = Carbon::now()->format('Y-m-d H:i:s');
        $products = [];
        for ($i = 0; $i < 30; $i++) { // Change 3 to the desired number of products
            $products[] = [
                "name" => $faker->word, // Change $faker->name to the appropriate faker method for product names
                "created_at" => $currentDateTime,
                "updated_at" => $currentDateTime,
            ];
        }
        
        Product::insert($products);
    }
}
