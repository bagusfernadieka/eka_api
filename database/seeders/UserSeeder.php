<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $currentDateTime = Carbon::now()->format('Y-m-d H:i:s');
        $products = [
            [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('asd'),
                'level' => 'admin',
                'api_token' => 'aaa',
                "created_at" => $currentDateTime,
                "updated_at" => $currentDateTime,
            ],
            [
                'name' => 'user',
                'email' => 'user@user.com',
                'password' => Hash::make('asd'),
                'level' => 'user',
                'api_token' => 'aaa',
                "created_at" => $currentDateTime,
                "updated_at" => $currentDateTime,
            ],
        ];
        
        User::insert($products);
    }
}
