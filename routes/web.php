<?php

use Illuminate\Support\Str;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('api/register', ['uses' => 'LoginController@register']);
$router->post('api/login', ['uses' => 'LoginController@login']);

$router->group( ['prefix' => 'api','middleware' => 'auth'], function() use ($router) {

    // user
    $router->get('user', ['uses' => 'UserController@index']);
    $router->get('user/{id}', ['uses' => 'UserController@show']);
    $router->post('user', ['uses' => 'UserController@create']);
    $router->put('user/{id}', ['uses' => 'UserController@update']);
    $router->delete('user/{id}', ['uses' => 'UserController@destroy']);

    // product
    $router->get('product', ['uses' => 'ProductController@index']);
    $router->get('product/{id}', ['uses' => 'ProductController@show']);
    $router->post('product', ['uses' => 'ProductController@create']);
    $router->put('product/{id}', ['uses' => 'ProductController@update']);
    $router->delete('product/{id}', ['uses' => 'ProductController@destroy']);
});