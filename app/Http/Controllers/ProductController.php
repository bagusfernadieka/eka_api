<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $query = Product::query();

            $skip = $request->query('skip', 0);
            $take = $request->query('take', 10);

            // Get total count before pagination
            $totalCount = $query->count();

            // Apply pagination
            $data = $query->skip($skip)->take($take)->get();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully loaded',
                'data' => $data,
                'totalCount' => $totalCount
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 422);
        }

        $data = [
            "name" => $request->name
        ];

        DB::beginTransaction();
        try {
            Product::create($data);
            DB::commit();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully created',
                'data' => null
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = Product::find($id);

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully loaded',
                'data' => $data
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 422);
        }

        $data = [
            "name" => $request->name
        ];

        DB::beginTransaction();
        try {
            // update data
            Product::find($id)->update($data);
            DB::commit();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully updated',
                'data' => null
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            // delete
            $status = Product::find($id)->status;
            if ($status == 'y') {
                Product::find($id)->update([
                    'status' => 'n'
                ]);
                DB::commit();

                return response()->json([
                    'code' => 200,
                    'message' => 'Data successfully deleted',
                    'data' => null
                ]);
            } else {
                // activate
                Product::find($id)->update([
                    'status' => 'y'
                ]);
                DB::commit();
                
                return response()->json([
                    'code' => 200,
                    'message' => 'Data successfully reactivated',
                    'data' => null
                ]);
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }
}
