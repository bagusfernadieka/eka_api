<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'email' => 'required|email|min:3||unique:users,email',
            'password' => 'required|string|min:3',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 422);
        }

        $currentDateTime = Carbon::now()->format('Y-m-d H:i:s');
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'level' => 'user',
            'api_token' => '12345678',
            "created_at" => $currentDateTime,
            "updated_at" => $currentDateTime,
        ];

        DB::beginTransaction();
        try {
            User::create($data);
            DB::commit();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully created',
                'data' => null
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 422);
        }

        DB::beginTransaction();
        try {
            $email = $request->input('email');
            $password = $request->input('password');

            $user = User::where('email', $email)->first();

            if (Hash::check($password, $user->password)) {
                $token = Str::random(40);

                $user->update([
                    "api_token" => $token
                ]);

                DB::commit();

                return response()->json([
                    'code' => 200,
                    'message' => 'Login successfull',
                    'data' => $user,
                    'token' => $token
                ]);

            } else {
                DB::rollBack();
                return response()->json([
                    'code' => 200,
                    'message' => 'Incorrect email or password',
                    'data' => null
                ]);
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }
}
