<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $query = User::query();

            $skip = $request->query('skip', 0);
            $take = $request->query('take', 10);

            // Get total count before pagination
            $totalCount = $query->count();

            // Apply pagination
            $data = $query->skip($skip)->take($take)->get();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully loaded',
                'data' => $data,
                'totalCount' => $totalCount
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'email' => 'required|email|min:3||unique:users,email',
            'password' => 'required|string|min:3',
            'level' => 'required|string|min:3',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 422);
        }

        $data = [
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            'level' => $request->level,
            'api_token' => '12345678',
        ];

        DB::beginTransaction();
        try {
            User::create($data);
            DB::commit();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully created',
                'data' => null
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = User::find($id);

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully loaded',
                'data' => $data
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'email' => 'required|email|min:3||unique:users,email,' . $id,
            'password' => 'required|string|min:3',
            'level' => 'required|string|min:3',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 422);
        }

        $data = [
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "level" => $request->level,
        ];

        DB::beginTransaction();
        try {
            // update data
            User::find($id)->update($data);
            DB::commit();

            return response()->json([
                'code' => 200,
                'message' => 'Data successfully updated',
                'data' => null
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            // delete
            $status = User::find($id)->status;
            if ($status == 'y') {
                User::find($id)->update([
                    'status' => 'n'
                ]);
                DB::commit();

                return response()->json([
                    'code' => 200,
                    'message' => 'Data successfully deleted',
                    'data' => null
                ]);
            } else {
                // activate
                User::find($id)->update([
                    'status' => 'y'
                ]);
                DB::commit();
                
                return response()->json([
                    'code' => 200,
                    'message' => 'Data successfully reactivated',
                    'data' => null
                ]);
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->errorInfo,
                'data' => null
            ]);
        }
    }
}
